import React, { Component, Fragment } from 'react';
import logo from './logo.png';
import './components/App.css';
import Affiche from './components/affiche';
import { Row, Col } from 'antd';
import Nouveautes from './components/nouveautes';
import Suggestions from './components/suggestions';

export default class App extends Component {

  state = {
    listMovies: []
  };

  async moviesExtract() {
    let response = await fetch("http://api.elorri.fr/disney-plus/movies")
                          .then(response => response.json());
    // let data = await response.json();
    // console.log("API response")
    // console.log(data);
    this.setState({
      listMovies: response
    });

  }

  componentDidMount() {
    this.moviesExtract();
  }




  //Préparation affichage image bannière

  render() {
    console.log("LIST MOVIES STATE")
    console.log(this.state.listMovies);
    let banMovies = this.state.listMovies.filter((movie) => movie.highlighted);
    let banAffiche = banMovies.map(movie => {
      return(
        <Affiche
            key={movie.id}
            id={movie.id}
            highlighted={movie.highlighted}
            cover={movie.cover}
        />
      )
    });

    //tirage au sort des posters pour encart Nouveautés


    const posters = [...this.state.listMovies];
    const tirageResult = [];
    if (posters.length > 0) {
      for (let i = 0; i < 6; i++) {

        let tirage = Math.floor(Math.random() * posters.length)
        tirageResult.push({id: posters[tirage].id, poster: posters[tirage].poster})
        
        posters.splice(tirage, 1)
        console.log("coucou")
        console.log(tirageResult)
      }
      
    }

    const affichagePosters = tirageResult.map((film) => {
      return (
        <Nouveautes
         link={`/movie/${film.id}`}
         new={film.poster}
        />

      )
    })


    // tirage au sort pour suggestions

    const affiche = [...this.state.listMovies];
    const selectionAffiches = [];
    console.log(affiche);

    if (posters.length > 0) {
      for (let j = 0; j < 3; j++) {

        let index = Math.floor(Math.random() * affiche.length)
        selectionAffiches.push(affiche[index].cover)
        affiche.splice(index, 1)

      }
      console.log(selectionAffiches);
    }





    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        
        {banAffiche}
        

        <img src="/" alt="" />

        <div className="btn">
          <button><img src={`${process.env.PUBLIC_URL}/img/companies/logo-disney.png`} alt="" /></button>
          <button><img src={`${process.env.PUBLIC_URL}/img/companies/logo-pixar.png`} alt="" /></button>
          <button><img src={`${process.env.PUBLIC_URL}/img/companies/logo-marvel.png`} alt="" /></button>
          <button><img src={`${process.env.PUBLIC_URL}/img/companies/logo-starwars.png`} alt="" /></button></div>
          
          
          <h2>Nouveautés</h2>
           <div className="newFilms">
          
          
       { affichagePosters }
       </div>


        <Suggestions
          sug1={selectionAffiches[0]}
          sug2={selectionAffiches[1]}
          sug3={selectionAffiches[2]}
        />
      </div>





    )
  }
}








