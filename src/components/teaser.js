import React from 'react';
import { Modal } from 'antd';

const Teaser = ({video, isVisible}) => {
    
    let iframeVisible = isVisible ? "visible" : "";
    return (
        <div>
        <Modal>
            <iframe src={video} className={iframeVisible} allow="fullscreen" ></iframe>
        </Modal>
        </div>
    )
}

export default Teaser;
